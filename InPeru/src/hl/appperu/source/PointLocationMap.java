package hl.appperu.source;
import hl.appperu.app.R;

import java.util.ArrayList;
import java.util.HashMap;
import android.graphics.Point;

public class PointLocationMap {
	HashMap<Integer,ArrayList<Point>> Place;
	HashMap<Integer,String> NameDep;
	public PointLocationMap() {
		Place=new HashMap<Integer, ArrayList<Point>>();
		NameDep=new HashMap<Integer, String>();
	}
	public HashMap<Integer,String> getNameMap(){
		return NameDep;
	}
	public HashMap<Integer,ArrayList<Point>> getMapPosition(){
		ArrayList<Point> Puno=new ArrayList<Point>();
		Puno.add(new Point(432,610));Puno.add(new Point(440,516));
		Puno.add(new Point(398,519));Puno.add(new Point(391,573));
		Puno.add(new Point(432,610));
		Place.put(R.drawable.b, Puno);
		NameDep.put(R.drawable.b, "Puno");
		
		ArrayList<Point> MadreDeDios=new ArrayList<Point>();
		MadreDeDios.add(new Point(450,476));MadreDeDios.add(new Point(382,401));
		MadreDeDios.add(new Point(346,437));MadreDeDios.add(new Point(386,480));
		MadreDeDios.add(new Point(450,476));
		Place.put(R.drawable.c, MadreDeDios);
		NameDep.put(R.drawable.c, "MadreDeDios");
		
		ArrayList<Point> Ucayali=new ArrayList<Point>();
		Ucayali.add(new Point(358,387));Ucayali.add(new Point(307,345));
		Ucayali.add(new Point(231,314));Ucayali.add(new Point(277,392));
		Ucayali.add(new Point(358,387));
		Place.put(R.drawable.d, Ucayali);
		NameDep.put(R.drawable.d, "Ucayali");
		
		ArrayList<Point> Loreto=new ArrayList<Point>();
		Loreto.add(new Point(256,26));Loreto.add(new Point(174,123));Loreto.add(new Point(165,180));
		Loreto.add(new Point(241,217));Loreto.add(new Point(222,265));Loreto.add(new Point(311,175));
		Loreto.add(new Point(364,127));Loreto.add(new Point(299,99));Loreto.add(new Point(256,26));
		Place.put(R.drawable.e, Loreto);
		NameDep.put(R.drawable.e, "Loreto");
		
		ArrayList<Point> Tacna=new ArrayList<Point>();
		Tacna.add(new Point(313,394));
		Tacna.add(new Point(430,654));
		Tacna.add(new Point(369,663));
		Tacna.add(new Point(313,394));
		Place.put(R.drawable.f, Tacna);
		NameDep.put(R.drawable.f, "Tacna");
		
		ArrayList<Point> Ilo=new ArrayList<Point>();
		Ilo.add(new Point(353,627));
		Ilo.add(new Point(366,598));
		Ilo.add(new Point(408,612));
		Ilo.add(new Point(372,660));
		Ilo.add(new Point(353,627));
		Place.put(R.drawable.g, Ilo);
		NameDep.put(R.drawable.g, "Ilo");
		
		ArrayList<Point> Cuzco=new ArrayList<Point>();
		Cuzco.add(new Point(367,560));
		Cuzco.add(new Point(344,517));
		Cuzco.add(new Point(304,484));
		Cuzco.add(new Point(318,437));
		Cuzco.add(new Point(375,506));
		Cuzco.add(new Point(367,560));
		Place.put(R.drawable.h, Cuzco);
		NameDep.put(R.drawable.h, "Cuzco");
		
		ArrayList<Point> Arequipa=new ArrayList<Point>();
		Arequipa.add(new Point(353,627));
		Arequipa.add(new Point(366,598));
		Arequipa.add(new Point(336,560));
		Arequipa.add(new Point(268,592));
		Arequipa.add(new Point(353,627));
		Place.put(R.drawable.i, Arequipa);
		NameDep.put(R.drawable.i, "Arequipa");
		
		ArrayList<Point> Ayacucho=new ArrayList<Point>();
		Ayacucho.add(new Point(297,562));
		Ayacucho.add(new Point(266,488));
		Ayacucho.add(new Point(244,527));
		Ayacucho.add(new Point(268,564));
		Ayacucho.add(new Point(297,562));
		Place.put(R.drawable.j, Ayacucho);
		NameDep.put(R.drawable.j, "Ayacucho");
		
		ArrayList<Point> Ica=new ArrayList<Point>();
		Ica.add(new Point(231,565));
		Ica.add(new Point(235,524));
		Ica.add(new Point(210,499));
		Ica.add(new Point(182,535));
		Ica.add(new Point(231,565));
		Place.put(R.drawable.k, Ica);
		NameDep.put(R.drawable.k, "Ica");
		
		ArrayList<Point> Huancavelica=new ArrayList<Point>();
		Huancavelica.add(new Point(225,514));
		Huancavelica.add(new Point(253,481));
		Huancavelica.add(new Point(234,479));
		Huancavelica.add(new Point(216,490));
		Huancavelica.add(new Point(225,514));
		Place.put(R.drawable.l, Huancavelica);
		NameDep.put(R.drawable.l, "Huancavelica");
		
		ArrayList<Point> Lima=new ArrayList<Point>();
		Lima.add(new Point(187,486));
		Lima.add(new Point(211,471));
		Lima.add(new Point(169,394));
		Lima.add(new Point(150,401));
		Lima.add(new Point(187,486));
		Place.put(R.drawable.m, Lima);
		NameDep.put(R.drawable.m, "Lima");
		
		ArrayList<Point> Huancayo=new ArrayList<Point>();
		Huancayo.add(new Point(214,444));
		Huancayo.add(new Point(269,415));
		Huancayo.add(new Point(265,410));
		Huancayo.add(new Point(195,425));
		Huancayo.add(new Point(214,444));
		Place.put(R.drawable.n, Huancayo);
		NameDep.put(R.drawable.n, "Huancayo");
		
		ArrayList<Point> CerroDePasco=new ArrayList<Point>();
		Place.put(R.drawable.o, CerroDePasco);
		
		ArrayList<Point> Ancash=new ArrayList<Point>();
		Ancash.add(new Point(125,392));
		Ancash.add(new Point(169,368));
		Ancash.add(new Point(155,315));
		Ancash.add(new Point(116,343));
		Ancash.add(new Point(125,392));
		Place.put(R.drawable.p, Ancash);
		NameDep.put(R.drawable.p, "Ancash");
		
		ArrayList<Point> Cajamarca=new ArrayList<Point>();
		Cajamarca.add(new Point(126,269));
		Cajamarca.add(new Point(115,183));
		Cajamarca.add(new Point(95,211));
		Cajamarca.add(new Point(84,267));
		Cajamarca.add(new Point(126,269));
		Place.put(R.drawable.q, Cajamarca);
		NameDep.put(R.drawable.q, "Cajamarca");
		
		ArrayList<Point> LaLibertad=new ArrayList<Point>();
		LaLibertad.add(new Point(114,313));
		LaLibertad.add(new Point(81,268));
		LaLibertad.add(new Point(108,328));
		LaLibertad.add(new Point(114,313));
		Place.put(R.drawable.r, LaLibertad);
		NameDep.put(R.drawable.r, "La Libertad");
		
		ArrayList<Point> Lambayeque=new ArrayList<Point>();
		Place.put(R.drawable.s, Lambayeque);
		
		ArrayList<Point> Piura=new ArrayList<Point>();
		Piura.add(new Point(48,219));
		Piura.add(new Point(84,194));
		Piura.add(new Point(63,168));
		Piura.add(new Point(32,180));
		Piura.add(new Point(48,219));
		Place.put(R.drawable.t, Piura);
		NameDep.put(R.drawable.t, "Piura");
		
		ArrayList<Point> Tumbes=new ArrayList<Point>();
		Place.put(R.drawable.u, Tumbes);
		
		ArrayList<Point> SanMartin=new ArrayList<Point>();
		SanMartin.add(new Point(200,307));
		SanMartin.add(new Point(218,239));
		SanMartin.add(new Point(174,220));
		SanMartin.add(new Point(165,261));
		SanMartin.add(new Point(200,307));
		Place.put(R.drawable.v, SanMartin);
		NameDep.put(R.drawable.v, "San Martin");
		
		ArrayList<Point> Amazonas=new ArrayList<Point>();
		Amazonas.add(new Point(143,245));
		Amazonas.add(new Point(154,132));
		Amazonas.add(new Point(118,163));
		Amazonas.add(new Point(117,199));
		Amazonas.add(new Point(143,245));
		Place.put(R.drawable.w, Amazonas);
		NameDep.put(R.drawable.w, "Amazonas");
		
		ArrayList<Point> Huanuco=new ArrayList<Point>();
		Huanuco.add(new Point(203,381));
		Huanuco.add(new Point(244,348));
		Huanuco.add(new Point(201,329));
		Huanuco.add(new Point(173,354));
		Huanuco.add(new Point(203,381));
		Place.put(R.drawable.y, Huanuco);
		NameDep.put(R.drawable.y, "Huanuco");
		
		return Place;
	}
	
	// b Puno
	// c MadreDeDios
	// d Ucayali
	// e Loreto
	// f Tacna
	// g Ilo
	// h Cuzco
	// i Arequipa
	//j Ayacucho
	//k ica
	//l Huancavelica
	//m Lima
	//n Huancayo
	//o CerroDePasco
	//p Ancash
	//q Cajamarca
	//r LaLibertad
	//s Lambayeque
	//t Piura
	//u Tumbes
	//v SanMatin
	//w Amazonas
}