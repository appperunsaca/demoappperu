package hl.appperu.app;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {
	TextView tvNday;
	TextView tvDay;
	SeekBar barDay;
	Button btnAccept;
	int NDAYS=1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);
		int alpha = 75;
		Typeface typeFace=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Light.ttf");
		
		tvNday=((TextView)findViewById(R.id.tvNday));
		tvNday.setTextColor(Color.argb(alpha, 255, 255, 255));
		tvNday.setTypeface(typeFace);
		tvDay=((TextView)findViewById(R.id.tvDay));
		tvDay.setTextColor(Color.argb(alpha, 255, 255, 255));
		tvDay.setTypeface(typeFace);
		barDay = (SeekBar)findViewById(R.id.seekBarND);
		barDay.setProgressDrawable(getResources().getDrawable(R.drawable.seek_bar_days));
		barDay.setProgress(0);
		barDay.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {seekBar.setThumb(getResources().getDrawable(R.drawable.button2));}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {seekBar.setThumb(getResources().getDrawable(R.drawable.button));}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
				int p=seekBar.getProgress();
				int numdays=14;int totP=100;
				p=((p*numdays/totP))+1;
				tvNday.setText(p<10?"0"+p:""+p);
				tvDay.setText(p>1?"DAYS":"DAY");
				NDAYS=p;
			}
		});
		btnAccept=((Button)findViewById(R.id.btnAccept));
		btnAccept.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i=new Intent(MainActivity.this, MapActivity.class);
				Bundle b=new Bundle();
				b.putInt("days", NDAYS);
				i.putExtras(b);
                startActivity(i);
//                finish();
			}
		});
	}
}