package hl.appperu.app;

import hl.appperu.source.PointLocationMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import android.R.layout;
import android.support.v7.app.ActionBarActivity;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class MapActivity extends ActionBarActivity {
	TextView tvMsjMap;
	TextView tvPlace;
	SeekBar barDay;
	HashMap<Integer,ArrayList<Point>> pointMap;
	HashMap<Integer,String> nameMap;
	RelativeLayout layoutMap;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_map);
		Bundle map=getIntent().getExtras();
		int NDAYS=map.getInt("days");
		Typeface typeFace=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-LightItalic.ttf");
		tvPlace=((TextView)findViewById(R.id.tvPlace));
		tvPlace.setText("PERU");
		tvPlace.setTypeface(typeFace);
		tvMsjMap=((TextView)findViewById(R.id.tvtMsjMap));
		tvMsjMap.setTypeface(typeFace);
		PointLocationMap mp=new PointLocationMap();
		pointMap=mp.getMapPosition();
		nameMap=mp.getNameMap();
		layoutMap=(RelativeLayout)findViewById(R.id.layoutMap);
//		layoutMap.setOnDragListener(new OnDragListener() {
//			@Override
//			public boolean onDrag(View v, DragEvent event) {
//				String pos = event.getX()+" , "+event.getY();
//				tvPlace.setText(pos);
//				int r =Poly(new Point((int)event.getX(),(int)event.getY()));
//				if(r==-1){
//					layoutMap.setBackground(getResources().getDrawable(R.drawable.a));
//				}else{
//					layoutMap.setBackground(getResources().getDrawable(r));
//				}
//				return false;
//			}
//		});
		layoutMap.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int r =Poly(new Point((int)event.getX(),(int)event.getY()));
				if(r==-1){
					layoutMap.setBackground(getResources().getDrawable(R.drawable.a));
				}else{
					tvPlace.setText(nameMap.get(r));
					layoutMap.setBackground(getResources().getDrawable(r));
				}
				return false;
			}
		});
	}
	private int Poly(Point P){
		int res=-1;
	    Set<Integer> K=pointMap.keySet();
	    for (Integer key : K) {
	    	int n=pointMap.size(), cn = 0;
		    ArrayList<Point> D=pointMap.get(key);
		    n=D.size()-1;
		    for (int i=0; i<n; i++) {
		       if (((D.get(i).y <= P.y) && (D.get(i+1).y > P.y))
		        || ((D.get(i).y > P.y) && (D.get(i+1).y <=  P.y))) {
		            float vt = (float)(P.y  - D.get(i).y) / (D.get(i+1).y - D.get(i).y);
		            if (P.x <  D.get(i).x + vt * (D.get(i+1).x - D.get(i).x))
		                 ++cn;
		        }
		    }
		    if((cn&1)==1)return key;
	    }
	    return -1;
	}
}