package com.aphidmobile.flip.demo;
import android.app.Activity;
import android.os.Bundle;
import com.aphidmobile.flip.FlipViewController;
import com.aphidmobile.flip.demo.adapter.TravelAdapter;
import com.aphidmobile.flipview.demo.R;
public class FlipHorizontalLayoutActivity extends Activity {

  private FlipViewController flipView;
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setTitle(R.string.activity_title);
    flipView = new FlipViewController(this, FlipViewController.HORIZONTAL);
    flipView.setAdapter(new TravelAdapter(this));
    setContentView(flipView);
  }

  @Override
  protected void onResume() {
    super.onResume();
    flipView.onResume();
  }

  @Override
  protected void onPause() {
    super.onPause();
    flipView.onPause();
  }
}
