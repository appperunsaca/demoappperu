/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.exercise.FragmentTest;

public final class R {
    public static final class attr {
        /** <p>Must be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>ARGB_8888</code></td><td>0</td><td></td></tr>
<tr><td><code>ARGB_4444</code></td><td>1</td><td></td></tr>
<tr><td><code>RGB_565</code></td><td>2</td><td></td></tr>
</table>
         */
        public static final int animationBitmapFormat=0x7f010001;
        /** <p>Must be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>vertical</code></td><td>0</td><td></td></tr>
<tr><td><code>horizontal</code></td><td>1</td><td></td></tr>
</table>
         */
        public static final int orientation=0x7f010000;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 
         */
        public static final int activity_horizontal_margin=0x7f070000;
        public static final int activity_vertical_margin=0x7f070001;
        public static final int textSize=0x7f070002;
    }
    public static final class drawable {
        public static final int ic_action_search=0x7f020000;
        public static final int ic_launcher=0x7f020001;
    }
    public static final class id {
        public static final int ARGB_4444=0x7f040003;
        public static final int ARGB_8888=0x7f040002;
        public static final int RGB_565=0x7f040004;
        public static final int horizontal=0x7f040001;
        public static final int parentFr=0x7f040005;
        public static final int vertical=0x7f040000;
    }
    public static final class layout {
        public static final int fragmentlayout=0x7f030000;
        public static final int main=0x7f030001;
    }
    public static final class string {
        public static final int app_name=0x7f050000;
        public static final int hello=0x7f050001;
    }
    public static final class style {
        public static final int AppTheme=0x7f060000;
    }
    public static final class styleable {
        /** Attributes that can be used with a FlipViewController.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #FlipViewController_animationBitmapFormat com.exercise.FragmentTest:animationBitmapFormat}</code></td><td></td></tr>
           <tr><td><code>{@link #FlipViewController_orientation com.exercise.FragmentTest:orientation}</code></td><td></td></tr>
           </table>
           @see #FlipViewController_animationBitmapFormat
           @see #FlipViewController_orientation
         */
        public static final int[] FlipViewController = {
            0x7f010000, 0x7f010001
        };
        /**
          <p>This symbol is the offset where the {@link com.exercise.FragmentTest.R.attr#animationBitmapFormat}
          attribute's value can be found in the {@link #FlipViewController} array.


          <p>Must be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>ARGB_8888</code></td><td>0</td><td></td></tr>
<tr><td><code>ARGB_4444</code></td><td>1</td><td></td></tr>
<tr><td><code>RGB_565</code></td><td>2</td><td></td></tr>
</table>
          @attr name com.exercise.FragmentTest:animationBitmapFormat
        */
        public static final int FlipViewController_animationBitmapFormat = 1;
        /**
          <p>This symbol is the offset where the {@link com.exercise.FragmentTest.R.attr#orientation}
          attribute's value can be found in the {@link #FlipViewController} array.


          <p>Must be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>vertical</code></td><td>0</td><td></td></tr>
<tr><td><code>horizontal</code></td><td>1</td><td></td></tr>
</table>
          @attr name com.exercise.FragmentTest:orientation
        */
        public static final int FlipViewController_orientation = 0;
    };
}
