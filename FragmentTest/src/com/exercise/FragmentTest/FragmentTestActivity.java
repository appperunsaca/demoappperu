package com.exercise.FragmentTest;
import java.util.ArrayList;

import android.R.layout;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.LinearLayout;

public class FragmentTestActivity extends Activity{
	
	Fragment fragment;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        ArrayList<String> list=new ArrayList<String>();
        list.add("AAA");list.add("BBB");list.add("CCC");list.add("DDDD");
		//parentFr
        LinearLayout linearMain = (LinearLayout) findViewById(R.id.parentFr);
		
//        
//        checkBox = new CheckBox(new ContextThemeWrapper(TabletActivity.this,R.style.chkCategoryStyle));
//		linearMain.addView(checkBox);
        
        
        int id=10003;
		for (String st : list) {
			LinearLayout lLayour = new LinearLayout(this);
			
			FragmentManager fragmentManager = getFragmentManager();
	        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	        lLayour.setId((id++));
	        linearMain.addView(lLayour);

	        FlipTextViewFragment myFragment = new FlipTextViewFragment();

//	        LayoutParams lp=new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//	        myFragment.getView().setLayoutParams(lp);  
	        fragmentTransaction.add(lLayour.getId(), myFragment);
	        fragmentTransaction.commit();
	        
//	        
		}
    }
}